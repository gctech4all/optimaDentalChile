from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^$', views.BuildingTemplateVew.as_view(), name='index'),
]
