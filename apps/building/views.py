from django.views.generic import TemplateView
from django.contrib.auth import authenticate, login
from django.shortcuts import HttpResponse, redirect, render


class BuildingTemplateVew(TemplateView):
    template_name = 'building/index.html'

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        if request.user.is_authenticated():
            return redirect('/')
        else:
            return self.render_to_response(context)


    def post(self, request, *args, **kwargs):
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('/')

        message = "Error de login"
        return self.render_to_response(self.get_context_data(message=message))
